package HAI501I.GestionTER;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;

class TestGestionTERSujet {
	Sujet sujetcomplet = new Sujet("sc", "Le titre du sujet complet");
	Sujet sujetvide = new Sujet();
	Sujet sujetaremplir = new Sujet();
	
	@Test
	public void testToStringSujet() {
		assertEquals("Le titre du sujet complet (id : sc)", sujetcomplet.toString());
		assertEquals(sujetcomplet.getTitre() + " (id : " + sujetcomplet.getId() + ")", sujetcomplet.toString());
		assertEquals("null (id : null)", sujetvide.toString());
		assertEquals(sujetvide.getTitre() + " (id : " + sujetvide.getId() + ")", sujetvide.toString());
		sujetaremplir.setTitre("Le titre du sujet a remplir");
		assertEquals("Le titre du sujet a remplir (id : null)", sujetaremplir.toString());
		
	}
	
	@Test
	public void testSerialisationSujet() throws StreamWriteException, DatabindException, IOException {
		Sujet sujetser = new Sujet("sser", "Sujet à sérialiser");
		sujetser.serSujet();
		File fichier = new File("target/sujet"+sujetser.getId()+".json");
	    Scanner lecture = new Scanner(fichier);
	    String l = lecture.nextLine();
	    assertEquals(l, "{\"id\":\"sser\",\"titre\":\"Sujet à sérialiser\"}");
	    lecture.close();
	}
	
	@Test
	public void testDeserialisationSujet() throws StreamWriteException, DatabindException, IOException {
		FileWriter ecriture = new FileWriter("target/sujettest.json");
	    ecriture.write("{\"id\":\"test\",\"titre\":\"Sujet à désérialiser\"}");
		ecriture.close();
		Sujet sujetdeser = new Sujet();
		sujetdeser.deserSujet("sujettest.json");
		assertEquals("test", sujetdeser.getId());
		assertEquals("Sujet à désérialiser", sujetdeser.getTitre());
	}
}
