package HAI501I.GestionTER;
import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Groupe {
	private String id;
	private String nom;
	private String idSujet;
	
	public Groupe() {
		this.id = null;
		this.nom = null;
		this.idSujet = null;
	}
		
	public Groupe(String id, String nom, String idSujet) {
		this.id = id;
		this.nom = nom;
		this.idSujet = idSujet;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public String getIdSujet() {
	    return this.idSujet;
	  }
		
	public void setVoeux(String idSujet) {
		this.idSujet = idSujet;
	}
	
	public void serGroupe() throws StreamWriteException, IOException {
		File fGroupe = new File("target/groupe"+this.id+".json");
		ObjectMapper omGrp = new ObjectMapper();
		omGrp.writeValue(fGroupe, this);
	}
	
	public void deserGroupe(String nomfichier) throws StreamWriteException, IOException {
		ObjectMapper omGrp = new ObjectMapper();
		String path = "target/"+nomfichier;
		Groupe grp = omGrp.readValue(new File(path), Groupe.class);
		this.id = grp.id;
		this.nom = grp.nom;
		this.idSujet = grp.idSujet;
	}
	
	public String toString() {
		String l = this.nom + " (id : " + this.id + ")\n";
		l += "Le sujet favori du groupe est le : ";
		l += this.idSujet;
		
		return l;
	}
}