package HAI501I.GestionTER;
/*
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GestionTER {
	public static void main(String[] args) throws StreamWriteException, DatabindException, IOException {
		
		// Instanciations de sujets
		Sujet s1 = new Sujet("1", "La reproduction des crevettes rouges");
	    Sujet s2 = new Sujet("2", "Les gnomes et leur mode de vie");
	    Sujet s3 = new Sujet("3", "L'utilisation des champignon dans la cuisine moderne");
	    Sujet s4 = new Sujet("4", "La fraise Tagada et ses dérivées");
	    Sujet s5 = new Sujet("5", "L'impact de ce TP sur le QI des élèves");
	    Sujet s6 = new Sujet("6", "La poésie au XIIIe siècle");
	    Sujet s7 = new Sujet("7", "Le lien entre mathématiques et orrthodontie");
	    Sujet s8 = new Sujet("8", "La culture des petits pois en Espagne Ouest");
	    Sujet s9 = new Sujet("9", "L'ascencion du Mont Blanc en chaussettes");
	    Sujet s10 = new Sujet("10", "Le sujet du TER d'un groupe");
	    
	    // Instanciations de groupes
	    Groupe g1 = new Groupe("1", "Groupe 1", "1");
	    Groupe g2 = new Groupe("2", "Groupe 2", "3");
	    Groupe g3 = new Groupe("3", "Groupe 3", "1");
	    Groupe g4 = new Groupe("4", "Groupe 4", "8");
	    Groupe g5 = new Groupe("5", "Groupe 5", "7");
	    
	    
	    // Création de deux listes, parce que c'est plus joli
	    ArrayList<Sujet> listeSujets = new ArrayList<Sujet>();
	    listeSujets.add(s1);
	    listeSujets.add(s2);
	    listeSujets.add(s3);
	    listeSujets.add(s4);
	    listeSujets.add(s5);
	    listeSujets.add(s6);
	    listeSujets.add(s7);
	    listeSujets.add(s8);
	    listeSujets.add(s9);
	    listeSujets.add(s10);
	    
	    ArrayList<Groupe> listeGroupes = new ArrayList<Groupe>();
	    listeGroupes.add(g1);
	    listeGroupes.add(g2);
	    listeGroupes.add(g3);
	    listeGroupes.add(g4);
	    listeGroupes.add(g5);
	    
	    
	    // Tests de sérialisations individuelles
	    s10.serSujet();
	    g5.serGroupe();
	    
	    // Tests de désérialisations individuelles
	    // Sujet s11 = new Sujet();
		// s11.deserSujet("sujet11.json");
		// System.out.println(s11.toString());
		
		// Groupe g6 = new Groupe();
		// g6.deserGroupe("groupe6.json");
		// System.out.println(g6.toString());
	    
	    // Sérialisation groupée
	    File fSujet = new File("target/sujets.json");
		ObjectMapper omSjt = new ObjectMapper();
		omSjt.writeValue(fSujet, listeSujets);
	    
		File fGroupe = new File("target/groupes.json");
		ObjectMapper omGrps = new ObjectMapper();
		omGrps.writeValue(fGroupe, listeGroupes);		
		
		Sujet sn = new Sujet();
		System.out.println(sn.toString());
	}
}
*/