/*

package HAI501I.GestionTER;

import java.util.ArrayList;
import java.util.List;

public class MatriceAffectation implements Hongrois {
	private int hauteur;
	private int largeur;
	private ArrayList<Sujet> listeSujets;
	private ArrayList<Groupe> listeGroupes;
	private int[][] m = new int[hauteur][largeur];
	
		public MatriceAffectation(int hauteur, int largeur, ArrayList<Sujet> listeSujets, ArrayList<Groupe> listeGroupes) {
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.listeSujets = listeSujets;
		this.listeGroupes = listeGroupes;
		
		int[][] minit = new int[this.hauteur][this.largeur];
		for (int i = 0; i < this.hauteur; i++) {
			for (int j = 0; j < this.largeur; j++) {
				minit[i][j] = -1;
			}
		}
		
		this.m = minit;
	}
	
	public int getHauteur() {
		return this.hauteur;
	}
	
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	
	public int getLargeur() {
		return this.largeur;
	}
	
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
		
	public int[][] getM() {
		return this.m;
	}
	
	public void setAdjacenceList() {
		for(int i = 0; i < this.largeur; i++) {
			Groupe g = listeGroupes.get(i);
			for(int j = 0; j < this.hauteur; j++) {
				Sujet s = listeSujets.get(j);
				for (int k = 0; k < 5; k++) {
					if (g.getPreference(k) == s) {
						m[i][j] = k;
					}
				}
			}
		}
	}
	
	public List<List<Integer>> affectation(int phaseNumber) {
		List<List<Integer>> aff = new ArrayList<List<Integer>>();
		ArrayList<Sujet> listeSujetsNA = (ArrayList<Sujet>) this.listeSujets.clone();
		ArrayList<Groupe> listeGroupesNA = (ArrayList<Groupe>) this.listeGroupes.clone();
		if (phaseNumber == 1) {
			for (int score = 0; score < 5; score++) {
				for (int j = 0; j < this.hauteur; j++) {
					if (listeSujetsNA.get(j) != null) {
						int nbscore = 0;
						int grpscore = 0;
						for (int i = 0; i < this.largeur; i++) {
							if (listeGroupesNA.get(i) != null) {
								if (m[i][j] == score ) {
									nbscore += 1;
									grpscore = i;
								}
							}
						}
						if (nbscore == 1) {
							List<Integer> aff_unit = new ArrayList<Integer>();
							aff_unit.add(grpscore);
							aff_unit.add(j);
							aff.add(aff_unit);
							listeGroupesNA.set(grpscore, null);
							listeSujetsNA.set(j, null);
						}
					}
					
				}
			}
		}
		if (phaseNumber == 2) {
			aff = affectation(1);
			for (int i = 0; i < this.largeur; i++) {
				if (listeGroupesNA.get(i) != null) {
					for (int j = 0; j < this.hauteur; j++) {
						if (listeSujetsNA.get(j) != null)
						{
							if (listeGroupesNA.get(i).getVoeux().contains(listeSujetsNA.get(j))) {
								List<Integer> aff_unit = new ArrayList<Integer>();
								aff_unit.add(i);
								aff_unit.add(j);
								aff.add(aff_unit);
								listeGroupesNA.set(i, null);
								listeSujetsNA.set(j, null);
							}
						}
					}
				}
			}
			for (int i = 0; i < this.largeur; i++) {
				if (listeGroupesNA.get(i) != null) {
					int j = 0;
					while (listeSujetsNA.get(j) == null && j < hauteur) {
						j += 1;		
						}
					List<Integer> aff_unit = new ArrayList<Integer>();
					aff_unit.add(i);
					aff_unit.add(j);
					aff.add(aff_unit);
					listeGroupesNA.set(i, null);
					listeSujetsNA.set(j, null);
					}
				}
		}
		return aff;
	}
}

*/
