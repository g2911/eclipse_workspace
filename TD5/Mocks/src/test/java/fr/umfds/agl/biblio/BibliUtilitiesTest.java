package fr.umfds.agl.biblio;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class BibliUtilitiesTest {
    @Mock
    GlobalBibliographyAccessInterface GBAMock;

    @InjectMocks
    BibliUtilities BU = new BibliUtilities();

    static Bibliotheque bib = Bibliotheque.getInstance();

    @BeforeAll
    static void init() {
        bib.addNotice(new NoticeBibliographique("ISBN-1", "Titre 1", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-2", "Titre 2", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-3", "Titre 3", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-4", "Titre 4", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-5", "Titre 5", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-6", "Titre 6", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-7", "Titre 7", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-8", "Titre 1", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-9", "Titre 2", "Pierre"));
        bib.addNotice(new NoticeBibliographique("ISBN-10", "Titre A", "Paul"));
        bib.addNotice(new NoticeBibliographique("ISBN-11", "Titre B", "Paul"));
        bib.addNotice(new NoticeBibliographique("ISBN-12", "Titre C", "Jacques"));
    }

    @Test
    void testNoticesConnexes() {
        NoticeBibliographique NB = bib.getNoticeByIsbn("ISBN-1");
        ArrayList<NoticeBibliographique> NBs = new ArrayList<>();
        NBs.add(bib.getNoticeByIsbn("ISBN-1"));
        NBs.add(bib.getNoticeByIsbn("ISBN-2"));
        NBs.add(bib.getNoticeByIsbn("ISBN-3"));
        NBs.add(bib.getNoticeByIsbn("ISBN-4"));
        NBs.add(bib.getNoticeByIsbn("ISBN-5"));
        NBs.add(bib.getNoticeByIsbn("ISBN-6"));
        NBs.add(bib.getNoticeByIsbn("ISBN-7"));
        NBs.add(bib.getNoticeByIsbn("ISBN-8"));
        NBs.add(bib.getNoticeByIsbn("ISBN-9"));

        doReturn(NBs).when(GBAMock).noticesDuMemeAuteurQue(NB);

        String[] possibleISBN = {"ISBN-2", "ISBN-3", "ISBN-4", "ISBN-5", "ISBN-6", "ISBN-7"};
        for (NoticeBibliographique NBC : BU.chercherNoticesConnexes(NB)) {
            assertTrue(Arrays.asList(possibleISBN).contains(NBC.getIsbn()));
        }
    }
}