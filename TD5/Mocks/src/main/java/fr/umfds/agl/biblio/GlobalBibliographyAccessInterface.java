package fr.umfds.agl.biblio;

import java.util.ArrayList;

public interface GlobalBibliographyAccessInterface {
    public NoticeBibliographique getNoticeFromIsbn(String isbn) throws IncorrectIsbnException;
    public ArrayList<NoticeBibliographique> noticesDuMemeAuteurQue(NoticeBibliographique ref);
    public ArrayList<NoticeBibliographique> autresEditions(NoticeBibliographique ref);
}
