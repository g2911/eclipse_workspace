package fr.umfds.agl.biblio;

public enum NoticeStatus {
	newlyAdded, updated, nochange, notExisting;
}
